#!/usr/bin/python3

import random
import csv
import sys
import collections

def get_people(filename='people.csv'):
    people = {}
    with open(filename) as f:
        reader = csv.DictReader(f)
        for row in reader:
            row['restrict'] = set(row['restrict'].split(';'))
            people[row['id']] = row
    return people

def valid(givers, receivers):
    restrictions = collections.defaultdict(set)
    for i, j in zip(givers, receivers):
        if i==j or j in people[i]['restrict'] or i in restrictions[j]:
           return False
        restrictions[j].update(people[i]['restrict'])
    return True

people = get_people(sys.argv[1])

givers = []
receivers = []
for id, person in people.items():
    for i in range(int(person.get('gives',1))):
        givers.append(id)
    for i in range(int(person.get('receives',1))):
        receivers.append(id)

random.seed()
while not valid(givers, receivers):
    random.shuffle(givers)

for x, y in zip(givers, receivers):
    px, py = people[x], people[y]
    print('%s,%s,%s,%s' % (px['name'], px['email'], py['name'], py['email']))
